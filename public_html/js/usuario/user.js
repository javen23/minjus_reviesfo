$(document).on('click','a[data-target=#crearUsuarioModal]',function(event)
{
    event.preventDefault();
    var target = $(this).attr("href");
    open_modal(target, 'crearUsuarioModal');
});

$(document).on('click','a[data-target=#editarUsuarioModal]',function(event)
{
    event.preventDefault();
    var target = $(this).attr("href");
    open_modal(target, 'editarUsuarioModal');
});

$(document).on('click',"#btnCreateUser",function()
{
    $("#form_crear_usuario").validate({
        errorClass  : 'myErrorClass',
        errorElement: 'div',
        rules: {
            nombres: "required",
            apellidos: "required",
            email:{
                required:true,
                email: true,
                remote: {
                    url: CI.base_url + "usuario/verifica",
                    type: "POST",
                    data: {
                        email: function () {
                            return $("input[name=email]").val();
                        }
                    }
                }
            },
            password: {
                required:true,
                minlength: 6
            }
        },
        messages: {
            nombres: "Ingrese Nombres",
            apellidos: "Ingrese Apellidos",
            email: {
                required: "Ingrese E-Mail",
                email: "Ingrese E-Mail correcto",
                remote: jQuery.validator.format("E-MAIL ya existe")
            },
            password:{
                required: "Ingrese contraseña",
                minlength: "Minimo 6 caracteres",
            }
        },
        submitHandler: function(form)
        {
            $("#btnCreateUser").addClass('disabled');
            ajax_user('crear');
        },
        highlight: function (element, required)
        {
            $(element).addClass('has-error-form');
        },
        unhighlight: function (element, errorClass, validClass)
        {
            $(element).removeClass('has-error-form');
        }
    });
});

function ajax_user(type_){
    $.ajax({
        url     : CI.base_url+'usuario/'+type_,
        type    : 'POST',
        data    : $("#form_"+type_+"_usuario").serializeArray(),
        dataType: 'json',
        beforeSend: function()
        {

        },
        success: function(response)
        {
            Notify(response.mensaje, 'top-right',response.tipo, response.icono);
            traer_listado_tabla('usuario/listado');
            $('#'+type_+'UsuarioModal').modal('hide');
        },
        error:function(err)
        {
            console.log(err);
        }
    });
}

$(document).on('click',"#btnEditarUsuario",function(){
    $("#form_editar_usuario").validate({
        errorClass: 'myErrorClass',
        rules: {
            nombres: "required",
            apellidos: "required",
            email:{
                required:true,
                email: true,
                remote: {
                    url: CI.base_url + "usuario/verifica",
                    type: "POST",
                    data: {
                        email: function () {
                            if($("input[name=email]").val() !== $("input[name=email_hidden]").val())
                            {
                                return ($("input[name=email]").val());
                            }
                        }
                    }
                }
            },
            password: {
                minlength: 6,
            }
        },
        messages: {
            nombres: "Ingrese Nombres",
            apellidos: "Ingrese Apellidos",
            email: {
                required: "Ingrese E-Mail",
                email: "Ingrese E-Mail correcto",
                remote: jQuery.validator.format("E-MAIL ya existe")
            },
            password: {
                minlength: "Minimo 6 caracteres"
            }
        },
        submitHandler: function () {
            $("#btnEditarUsuario").addClass('disabled');
            ajax_user('editar');
        },
        highlight: function (element, required)
        {
            $(element).addClass('has-error-form');
        },
        unhighlight: function (element, errorClass, validClass)
        {
            $(element).removeClass('has-error-form');
        }
    });
});