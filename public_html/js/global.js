function open_modal(target_, id_)
{
    $("#"+id_+" .modal-content").load(target_, function() {
        $("#"+id_).modal("show");
    });
}

function tablaListadoDataTable(id_tabla) {
    var tabla_id = id_tabla || "tablaListado";
    $('#'+tabla_id).dataTable({
        "language": {
            "search": "",
            "searchPlaceholder": "Buscar...",
            "sLengthMenu": 'Mostrando <select class="form-control input-sm">' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
            "oPaginate": {
                "sFirst"    : "<i class='fa fa-angle-double-left'></i>",
                "sLast"     : "<i class='fa fa-angle-double-right'></i>",
                "sNext"     : "<i class='fa fa-angle-right'></i>",
                "sPrevious" : "<i class='fa fa-angle-left'></i>"
            },
            "sInfoEmpty": "0 registros que mostrar",
            "sZeroRecords": "<div class='text-center'><i class='fa fa-frown-o'></i><h3>No hay registro que mostrar</h3></div>",
            "sLoadingRecords": "Por favor espere - cargando...",
            "sInfo": "mostrando _START_ de _END_ registros, total _TOTAL_ registros"
        },
    });
    var table = $("#"+tabla_id).DataTable();
    $('#'+tabla_id+' tfoot th').each(function () {
        var title = $('#'+tabla_id+' thead tr.th-head-inputs th').eq($(this).index()).text();
        $(this).html('<input type="text" placeholder="' + title + '" />');
    });
    table.columns().eq(0).each(function (colIdx) {
        $('input', table.column(colIdx).footer()).on('keyup change', function () {
            table.column(colIdx).search(this.value).draw();
        });
    });
}

$(document).on('click','.change_state',function(){
    var estado_ = $(this).data('estado');
    var id_     = $(this).data('id');
    var modulo_ = $(this).data('modulo');
    var data_ = {
        id      : id_,
        estado  : estado_
    };
    $.ajax({
        url     : CI.base_url+modulo_+'/estado',
        type    : 'POST',
        data    : data_,
        dataType: 'json',
        beforeSend: function()
        {
        },
        success: function(response)
        {
            Notify(response.mensaje, 'top-right',response.tipo, response.icono);
            traer_listado_tabla(modulo_+'/listado');
        },
        error: function (err)
        {
            console.log(err);
        }
    });
});

function traer_listado_tabla(ruta){
    var url = CI.base_url + ruta;
    $('#resultado_filtro').slideUp('high', function () {
        $(this).load(url, function () {
            tablaListadoDataTable();
            $(this).slideDown('slow');
        });
    });
}
