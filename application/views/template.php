<?php
    $id_login   = $this->session->userdata('user.id');
    if ($id_login):
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view($contenido);
        $this->load->view('template/footer');
    else:
        $this->load->view($contenido);
    endif;
