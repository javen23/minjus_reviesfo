<!-- Page Content -->
<div class="page-content">
    <!-- Page Breadcrumb -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url()?>">Inicio</a>
            </li>
            <li class="active">Evaluación</li>
        </ul>
    </div>
    <!-- /Page Breadcrumb -->
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                MODULO DE EVALUACIÓN DE LOS LINEAMIENTOS DE TRABAJO - INSCRIPCIÓN ITINERANTE
                <!--                <small>-->
<!--                    <i class="fa fa-angle-right"></i>-->
<!--                    jquery plugin for data management-->
<!--                </small>-->
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a data-toggle="modal" href="<?php echo base_url('personal/crear'); ?>" data-target="#crearPersonalModal">
                <i class="fa fa-plus"></i>
            </a>
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
<!--            <a class="refresh" id="refresh-toggler" href="#">-->
<!--                <i class="glyphicon glyphicon-refresh"></i>-->
<!--            </a>-->
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="page-body">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="widget">
                    <div class="widget-header ">
                        <span class="widget-caption">Listado del Personal</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="table-responsive row-none" id="resultado_filtro">
                            <table id="tablaListado" class="table table-striped table-bordered table-hover">
                                <thead class="headTablaListado">
                                    <tr class="text-uppercase text-center th-head-inputs">
                                        <th>N°</th>
                                        <th>DISTRITO JUDICIAL</th>
                                        <th>SEDE</th>
                                        <th>CARGO</th>
                                        <th>APELLIDOS Y NOMBRES</th>
                                        <th>EMAIL</th>
                                        <th>ACTUALIZADO</th>
                                        <th>ACCIÓN</th>
                                    </tr>
                                </thead>
                                <tfoot class="footTablaListado">
                                    <tr class="text-uppercase">
                                        <th>N°</th>
                                        <th>DISTRITO JUDICIAL</th>
                                        <th>SEDE</th>
                                        <th>CARGO</th>
                                        <th>APELLIDOS Y NOMBRES</th>
                                        <th>EMAIL</th>
                                        <th>ACTUALIZADO</th>
                                        <th>ACCIÓN</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php $n = 1; foreach ($all_personal as $personal) : ?>
                                <?php if($personal['id'] > 0):?>
                                    <tr>
                                        <td class="text-center"><?php echo $n++; ?></td>
                                        <td><?php echo $personal['distrito_descripcion'] ?></td>
                                        <td><?php echo $personal['sede_descripcion'] ?></td>
                                        <td><?php echo $personal['cargo_descripcion'] ?></td>
                                        <td><?php echo strtoupper($personal['apellido_paterno'] . " " . $personal['apellido_materno']. ", " . $personal['nombres']); ?></td>
                                        <td class="text-lowercase"><?php echo $personal['email'] ?></td>
                                        <td class="text-center"><?php echo $personal['creado_por'] ?></td>
                                        <td class="text-center">
                                            <span data-toggle="tooltip" data-placement="top" title="EDITAR">
                                                <a href="<?php echo base_url('personal/editar'); ?>?id_user=<?php echo $personal['id'] ?>"
                                                    data-target="#editarPersonalModal"
                                                    class='btn btn-success'>
                                                    <i class='fa fa-pencil'></i> Editar
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                <?php endif;?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" id="crearPersonalModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-darkorange">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                    <div class="modal fade" id="editarPersonalModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-darkorange">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        tablaListadoDataTable();
    });
</script>
