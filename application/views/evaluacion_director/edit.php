<!-- Page Content -->
<div class="page-content">
    <!-- Page Breadcrumb -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url()?>">Inicio</a>
            </li>
            <li class="active">Registro</li>
        </ul>
    </div>
    <!-- /Page Breadcrumb -->
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                REGISTRO DE VÍCTIMAS DE ESTERILIZACIONES FORZADAS (REVIESFO)
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <!--a data-toggle="modal" href="<php echo base_url('usuario/crear'); ?>" data-target="#crearUsuarioModal">
                <i class="fa fa-plus"></i>
            </a-->
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
            <!--a class="refresh" id="refresh-toggler" href="#">
                <i class="glyphicon glyphicon-refresh"></i>
            </a-->
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="page-body">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="widget flat radius-bordered">
                            <div class="widget-header bg-danger">
                                <span class="widget-caption">REGISTRO DE VÍCTIMAS DE ESTERILIZACIONES FORZADAS (REVIESFO)</span>
                            </div>
                            <div class="widget-body">
                                <div id="registration-form">
                                    <form role="form">
                                        <div class="form-title" align="center">
                                            <b>EVALUACIÓN DE LOS LINEAMIENTOS DE TRABAJO - INSCRIPCIÓN ITINERANTE </b><br/><i><?php echo $evaluacion_titulo[0]['evaluacion_descripcion']; ?></i>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <label for="lugar_editar">Lugar de Inscripción:</label>
                                                       <span class="input-icon icon-right">
                                                           <input type="text" class="form-control" id="lugar_editar" name="lugar_editar" placeholder="[Lugar de Inscripción]">
                                                           <i class="glyphicon glyphicon-briefcase darkorange"></i>
                                                       </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="fecha_editar">Fecha:</label>
                                                        <span class="input-icon icon-right">
                                                            <input type="text" class="form-control" id="fecha_editar" name="fecha_editar" placeholder="[Fecha]">
                                                            <i class="fa fa-calendar blue"></i>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="responsable_editar">Responsable:</label>
                                                       <span class="input-icon icon-right">
                                                           <input type="text" class="form-control" id="responsable_editar" name="responsable_editar" placeholder="[Responsable]" value="<?php echo $personal['cargo']; ?>" />
                                                           <input type="hidden" id="cargo_id_editar" name="cargo_id_editar" value="<?php echo $personal['cargo_id']; ?>"/>
                                                            <i class="glyphicon glyphicon-user palegreen"></i>
                                                       </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="direccion_distrital_editar">Dirección Distrital:</label>
                                                        <span class="input-icon icon-right">
                                                            <input type="text" class="form-control" id="direccion_distrital_editar" name="direccion_distrital_editar" placeholder="[Dirección Distrital]"  value="<?php echo $personal['distrito_judicial']; ?>" />
                                                            <input type="hidden" id="cargo_id_editar" name="cargo_id_editar" value="<?php echo $personal['distrito_judicial_id']; ?>"/>
                                                            <i class="glyphicon glyphicon-user palegreen"></i>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="nombre_editar">Nombre:</label>
                                                        <span class="input-icon icon-right">
                                                            <input type="text" class="form-control" id="nombre_editar" name="nombre_editar" placeholder="[Nombre]" value="<?php echo $personal['apellido_paterno'].' '.$personal['apellido_materno'].', '.$personal['nombres']; ?>" />
                                                            <i class="glyphicon glyphicon-user palegreen"></i>
                                                    </span>
                                                </div>
                                                <hr class="wide" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="well with-header  with-footer">
                                                    <div class="header bg-darkorange">
                                                        <div align="center"><?php echo $evaluacion_titulo[0]['titulo_descripicion']; ?></div>
                                                    </div>
                                                    <table class="table table-hover table-striped table-bordered table-condensed">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                Herramientas
                                                            </th>
                                                            <th>
                                                                Especificar
                                                            </th>
                                                            <th>
                                                                Medios de Verificación
                                                            </th>
                                                            <th>
                                                                Cumple
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($evaluacion_detalle as $detalle) { ?>

                                                            <?php if($detalle['titulo_id'] == 1) { ?>
                                                            <tr>
                                                                <td><?php echo $detalle['detalle_descripcion']?><br/> <?php echo $detalle['detalle_informativo']?></td>
                                                                <td><input type="text" class="form-control" id="especificar_editar_<?php echo $detalle['detalle_id']; ?>" name="especificar_editar_<?php echo $detalle['detalle_id']; ?>"  placeholder="[Especificar]" value="" /></td>
                                                                <td><input type="text" class="form-control" id="medios_editar_<?php echo $detalle['detalle_id']; ?>" name="medios_editar_<?php echo $detalle['detalle_id']; ?>"  placeholder="[Medios de Verifiación]" value="" /></td>
                                                                <td>
                                                                    <div class="radio">
                                                                        <label>
                                                                            <input type="radio" name="cumple_<?php echo $detalle['detalle_id']; ?>" disabled="disabled" value="1">
                                                                            <span class="text">SI</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio">
                                                                        <label>
                                                                            <input type="radio" name="cumple_<?php echo $detalle['detalle_id']; ?>" disabled="disabled" value="0">
                                                                            <span class="text">NO</span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <div class="well with-header  with-footer">
                                                    <div class="header bg-darkorange" >
                                                        <div align="center"><?php echo $evaluacion_titulo[1]['titulo_descripicion']; ?></div>
                                                    </div>
                                                    <table class="table table-hover table-striped table-bordered table-condensed">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                Actores Involucrados <br/>(Instituciones, Ministerios y Gremios)
                                                            </th>
                                                            <th>
                                                                Especificar
                                                            </th>
                                                            <th>
                                                                Cumple
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($evaluacion_detalle as $detalle) { ?>
                                                            <?php if($detalle['titulo_id'] == 2) { ?>
                                                                <tr>
                                                                    <td><?php echo $detalle['detalle_descripcion']?> <br/> <?php echo $detalle['detalle_informativo']?></td>
                                                                    <td><input type="text" class="form-control" id="especificar_editar_<?php echo $detalle['detalle_id']; ?>" name="especificar_editar_<?php echo $detalle['detalle_id']; ?>"  placeholder="[Especificar]" value="" /></td>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="cumple_<?php echo $detalle['detalle_id']; ?>" value="1" disabled="disabled">
                                                                                <span class="text">SI</span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="cumple_<?php echo $detalle['detalle_id']; ?>" value="0" disabled="disabled">
                                                                                <span class="text">NO</span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="well with-header  with-footer">
                                                    <div class="header bg-darkorange">
                                                        <?php echo $evaluacion_titulo[7]['titulo_descripicion']; ?>
                                                    </div>
                                                    <table class="table table-hover table-striped table-bordered table-condensed">
                                                        <thead>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($evaluacion_detalle as $detalle) { ?>
                                                            <?php if($detalle['titulo_id'] == 8) { ?>
                                                                <tr>
                                                                    <td><?php echo $detalle['detalle_descripcion']?></td>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="evaluacion_<?php echo $detalle['detalle_id']; ?>" value="1" >
                                                                                <span class="text">MALO</span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="evaluacion_<?php echo $detalle['detalle_id']; ?>" value="2" >
                                                                                <span class="text">REGULAR</span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="evaluacion_<?php echo $detalle['detalle_id']; ?>" value="3" >
                                                                                <span class="text">BUENO</span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="radio">
                                                                            <label>
                                                                                <input type="radio" name="evaluacion_<?php echo $detalle['detalle_id']; ?>" value="4" >
                                                                                <span class="text">MUY BUENO</span>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-danger">Grabar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /Page Body -->

</div>

