<div class="modal-header">
    <h4 class="modal-title text-center">CREAR USUARIO</h4>
</div>

<div class="modal-body">
    <form class="row p-10 verificando_form" role="form" autocomplete="off" id="form_crear_usuario" >
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="rol">Perfíl</label>
                    <select class="form-control b-radio" id="rol" name="rol">
                        <?php foreach ($all_rols as $rol) : ?>
                            <option value="<?php echo $rol['id'] ?>"><?php echo strtoupper($rol['nombre']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nombres">Nombres</label>
                    <input type="text" id="nombres" name="nombres" class="form-control text-uppercase" placeholder="INGRESE NOMBRES ">
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="apellidos">Apellidos</label>
                    <input type="text" id="apellidos" name="apellidos" class="form-control text-uppercase" placeholder="INGRESE APELLIDOS">
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="INGRESE E-MAIL ">
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="INGRESE CONTRASEÑA">
                </div>
            </div>
        </div>

        <div class="col-sm-12 text-center">
            <input name="dataUser" type="hidden" value="createUser"/>
            <button type="submit" class="btn btn-success waves-effect m-r-5" name="crearUsuario" id="btnCreateUser">CREAR</button>
            <button type="button" class="btn btn-danger waves-effect m-l-5" data-dismiss="modal" id="cancelarEventoModal">CANCELAR</button>
        </div>
    </form>
</div>