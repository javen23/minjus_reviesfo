<table id="tablaListado" class="table table-striped table-bordered table-hover">
    <thead class="headTablaListado">
    <tr class="text-uppercase text-center th-head-inputs">
        <th>N°</th>
        <th>PERFIL</th>
        <th>USUARIO</th>
        <th>APELLIDOS Y NOMBRES</th>
        <th>EMAIL</th>
        <th>ESTADO</th>
        <th>ACTUALIZADO</th>
        <th>ACCIÓN</th>
    </tr>
    </thead>
    <tfoot class="footTablaListado">
    <tr class="text-uppercase">
        <th>N°</th>
        <th>PERFIL</th>
        <th>USUARIO</th>
        <th>APELLIDOS Y NOMBRES</th>
        <th>EMAIL</th>
        <th>ESTADO</th>
        <th>ACTUALIZADO</th>
        <th>EDITAR</th>
    </tr>
    </tfoot>
    <tbody>
    <?php $n = 1; foreach ($all_users as $usuario) : ?>
        <?php if($usuario['id']>0):?>
        <tr>
            <td class="text-center"><?php echo $n++; ?></td>
            <td><?php echo $usuario['rol'] ?></td>
            <td><?php echo $usuario['usuario'] ?></td>
            <td><?php echo strtoupper($usuario['apellido_paterno'] . " " . $usuario['apellido_materno'] . ", ".$usuario['nombres']); ?></td>
            <td class="text-lowercase"><?php echo $usuario['email'] ?></td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="CAMBIAR ESTADO">
                    <a data-estado="<?php echo $usuario['estado'] ?>"
                       data-id="<?php echo $usuario['id'] ?>"
                       data-modulo="usuario"
                       class='change_state btn <?php echo ($usuario['estado'] == 1) ? " btn-success " : " btn-danger "; ?>  btn btn-default  btn-circle btn-xs'>
                        <?php echo verificar_estado($usuario['estado']) ?>
                    </a>
                </span>
            </td>
            <td class="text-center"><?php echo $usuario['creado_por'] ?></td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="EDITAR">
                    <a data-toggle="modal"
                       href="<?php echo base_url('usuario/editar'); ?>?id_user=<?php echo $usuario['id'] ?>"
                       data-target="#editarUsuarioModal"
                       class='btn btn-success'>
                        <i class='fa fa-pencil'></i> Editar
                    </a>
                </span>
            </td>
        </tr>
        <?php endif;?>
    <?php endforeach; ?>
    </tbody>
</table>