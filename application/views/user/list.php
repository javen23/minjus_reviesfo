<!-- Page Content -->
<div class="page-content">
    <!-- Page Breadcrumb -->
    <div class="page-breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url()?>">Inicio</a>
            </li>
            <li class="active">Usuarios</li>
        </ul>
    </div>
    <!-- /Page Breadcrumb -->
    <!-- Page Header -->
    <div class="page-header position-relative">
        <div class="header-title">
            <h1>
                Modulo Usuarios
<!--                <small>-->
<!--                    <i class="fa fa-angle-right"></i>-->
<!--                    jquery plugin for data management-->
<!--                </small>-->
            </h1>
        </div>
        <!--Header Buttons-->
        <div class="header-buttons">
            <a data-toggle="modal" href="<?php echo base_url('usuario/crear'); ?>" data-target="#crearUsuarioModal">
                <i class="fa fa-plus"></i>
            </a>
            <a class="sidebar-toggler" href="#">
                <i class="fa fa-arrows-h"></i>
            </a>
<!--            <a class="refresh" id="refresh-toggler" href="#">-->
<!--                <i class="glyphicon glyphicon-refresh"></i>-->
<!--            </a>-->
            <a class="fullscreen" id="fullscreen-toggler" href="#">
                <i class="glyphicon glyphicon-fullscreen"></i>
            </a>
        </div>
        <!--Header Buttons End-->
    </div>
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="page-body">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="widget">
                    <div class="widget-header ">
                        <span class="widget-caption">Listado de Usuarios</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="table-responsive row-none" id="resultado_filtro">
                            <table id="tablaListado" class="table table-striped table-bordered table-hover">
                                <thead class="headTablaListado">
                                    <tr class="text-uppercase text-center th-head-inputs">
                                        <th>N°</th>
                                        <th>PERFIL</th>
                                        <th>USUARIO</th>
                                        <th>APELLIDOS Y NOMBRES</th>
                                        <th>EMAIL</th>
                                        <th>ESTADO</th>
                                        <th>ACTUALIZADO</th>
                                        <th>ACCIÓN</th>
                                    </tr>
                                </thead>
                                <tfoot class="footTablaListado">
                                    <tr class="text-uppercase">
                                        <th>N°</th>
                                        <th>PERFIL</th>
                                        <th>USUARIO</th>
                                        <th>APELLIDOS Y NOMBRES</th>
                                        <th>EMAIL</th>
                                        <th>ESTADO</th>
                                        <th>ACTUALIZADO</th>
                                        <th>EDITAR</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php $n = 1; foreach ($all_users as $usuario) : ?>
                                <?php if($usuario['id']>0):?>
                                    <tr>
                                        <td class="text-center"><?php echo $n++; ?></td>
                                        <td><?php echo $usuario['rol'] ?></td>
                                        <td><?php echo $usuario['usuario'] ?></td>
                                        <td><?php echo strtoupper($usuario['apellido_paterno'] . " " . $usuario['apellido_materno'] . ", ".$usuario['nombres']) ?></td>
                                        <td class="text-lowercase"><?php echo $usuario['email'] ?></td>
                                        <td class="text-center">
                                            <span data-toggle="tooltip" data-placement="top" title="CAMBIAR ESTADO">
                                                <a data-estado="<?php echo $usuario['estado'] ?>"
                                                   data-id="<?php echo $usuario['id'] ?>"
                                                   data-modulo="usuario"
                                                   class='change_state btn <?php echo ($usuario['estado'] == 1) ? " btn-success " : " btn-danger "; ?>  btn btn-default  btn-circle btn-xs'>
                                                     <?php echo verificar_estado($usuario['estado']) ?>
                                                </a>
                                            </span>
                                        </td>
                                        <td class="text-center"><?php echo $usuario['creado_por'] ?></td>
                                        <td class="text-center">
                                            <span data-toggle="tooltip" data-placement="top" title="EDITAR">
                                                <a href="<?php echo base_url('usuario/editar'); ?>?id_user=<?php echo $usuario['id'] ?>"
                                                    data-target="#editarUsuarioModal"
                                                    class='btn btn-success'>
                                                    <i class='fa fa-pencil'></i> Editar
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                <?php endif;?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" id="crearUsuarioModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-darkorange">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                    <div class="modal fade" id="editarUsuarioModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-darkorange">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        tablaListadoDataTable();
    });
</script>
