<div class="modal-header">
    <h4 class="modal-title text-center">EDITAR USUARIO</h4>
</div>

<div class="modal-body">
    <form class="row p-10 verificando_form" role="form" autocomplete="off" id="form_editar_usuario" >
        <input type="hidden" name="hidden_id_user" value="<?php echo $user['id'] ?>">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="rol">Perfíl</label>
                    <select class="form-control b-radio" id="rol" name="rol">
                        <?php foreach ($all_rols as $rol) : ?>
                            <option value="<?php echo $rol['id'] ?>" <?php echo ($rol['id']===$user['tbl_rol_id']) ? "selected" : ""; ?>><?php echo strtoupper($rol['nombre']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nombres">Nombres</label>
                    <input type="text" id="nombres" name="nombres" class="form-control text-uppercase" value="<?php echo $user['nombres']?>" placeholder="INGRESE NOMBRES ">
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="apellidos">Apellidos</label>
                    <input type="text" id="apellidos" name="apellidos" class="form-control text-uppercase" value="<?php echo $user['apellidos']?>" placeholder="INGRESE APELLIDOS">
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="hidden" name="email_hidden" value="<?php echo $user['email'] ?>">
                    <input type="email" id="email" name="email" class="form-control" value="<?php echo $user['email']?>" placeholder="INGRESE E-MAIL ">
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="INGRESE CONTRASEÑA">
                </div>
            </div>
        </div>

        <div class="col-sm-12 text-center">
            <input name="dataUser" type="hidden" value="EditUser"/>
            <button type="submit" class="btn btn-success waves-effect m-r-5" name="editarUsuario" id="btnEditarUsuario">ACTUALIZAR</button>
            <button type="button" class="btn btn-danger waves-effect m-l-5" data-dismiss="modal" id="cancelarEventoModal">CANCELAR</button>
        </div>
    </form>
</div>