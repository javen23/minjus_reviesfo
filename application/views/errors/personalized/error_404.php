<div class="four-zero error_404">
    <h2><i class="zmdi zmdi-mood-bad zmdi-hc-lg animated infinite pulse"></i></h2>
    <small>NO ENCONTRAMOS LO QUE BUSCAS...</small>
    <footer>
        <a href="#" onclick="window.history.go(-1); return false;"><i class="zmdi zmdi-arrow-back"></i></a>
        <a href="http://sigmo.net.pe/"><i class="zmdi zmdi-home"></i></a>
    </footer>
</div>