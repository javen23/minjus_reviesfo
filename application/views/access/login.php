<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo (isset($titulo) && $titulo !== "") ? $titulo . " | " : ""; echo NAME_PROYECT ?></title>

        <link href="<?php echo base_url()?>public_html/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url()?>public_html/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url()?>public_html/css/beyond.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>public_html/css/demo.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>public_html/css/animate.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url()?>public_html/css/global-login.css" rel="stylesheet"/>
        <script src="<?php echo base_url()?>public_html/js/skins.min.js"></script>
    </head>
    <body>
    <div align="center"><img src="<?php echo base_url()?>public_html/img/logo_cab.png" width="890" height="125" border="0" usemap="#Map"></div>
        <!-- Login -->
        <div class="login-container animated fadeInDown">
            <form action="<?php echo base_url('login') ?>" novalidate="novalidate" method="POST" id="form_login_usuario" autocomplete="off">
                <div class="loginbox bg-white">

                    <div class="loginbox-title">Accesso al Sistema de REVIESFO</div>
                    <div class="loginbox-textbox">
                        <input type="text" class="form-control text-lowercase" placeholder="Usuario" name="sendUser"/>
                    </div>
                    <div class="loginbox-textbox">
                        <input type="password" class="form-control" placeholder="Contraseña" name="sendPass"/>
                    </div>
                    <div class="loginbox-submit">
                        <input type="submit" class="btn btn-primary btn-block" value="Ingresar">
                    </div>
                </div>
            </form>
            <div class="logobox">
                <p class="m-t-0">V.1.0</p>
            </div>
        </div>
        <!-- Fin Login -->
        <!-- Javascript Libraries -->
        <script src="<?php echo base_url()?>public_html/js/jquery.min.js"></script>
        <script src="<?php echo base_url()?>public_html/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>public_html/js/slimscroll/jquery.slimscroll.min.js"></script>
        <!--Scripts-->
        <script src="<?php echo base_url()?>public_html/js/beyond.js"></script>
        <script src="<?php echo base_url()?>public_html/js/toastr/toastr.js"></script>
        <?php
        $usuario_incorrecto = $this->session->flashdata('usuario_incorrecto');
        if ($usuario_incorrecto) : ?>
            <script type="text/javascript">
                $(function () {
                    Notify('<?php echo $usuario_incorrecto?>', 'top-right','danger', 'fa-close');
                });
            </script>
        <?php endif;?>
    </body>
</html>
