<div class="page-content">
    <!-- Page Breadcrumb -->
<!--    <div class="page-breadcrumbs">-->
<!--        <ul class="breadcrumb">-->
<!--            <li>-->
<!--                <i class="fa fa-home"></i>-->
<!--                <a href="#">Home</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="#">Elements</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="#">Icons</a>-->
<!--            </li>-->
<!--            <li class="active">Typicon</li>-->
<!--        </ul>-->
<!--    </div>-->
    <!-- /Page Breadcrumb -->
    <!-- Page Header -->
<!--    <div class="page-header position-relative">-->
<!--        <div class="header-title">-->
<!--            <h1>-->
<!--                Typicon-->
<!--                <small>-->
<!--                    <i class="fa fa-angle-right"></i>-->
<!--                    vector icons-->
<!--                </small>-->
<!--            </h1>-->
<!--        </div>-->
        <!--Header Buttons-->
<!--        <div class="header-buttons">-->
<!--            <a class="sidebar-toggler" href="#">-->
<!--                <i class="fa fa-arrows-h"></i>-->
<!--            </a>-->
<!--            <a class="refresh" id="refresh-toggler" href="#">-->
<!--                <i class="glyphicon glyphicon-refresh"></i>-->
<!--            </a>-->
<!--            <a class="fullscreen" id="fullscreen-toggler" href="#">-->
<!--                <i class="glyphicon glyphicon-fullscreen"></i>-->
<!--            </a>-->
<!--        </div>-->
        <!--Header Buttons End-->
<!--    </div>-->
    <!-- /Page Header -->
    <!-- Page Body -->
    <div class="page-body">
        <div class="horizontal-space space-sm"></div>

        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="widget">

                    <div class="widget-header bordered-top bordered-pink">
                        <i class="widget-icon fa fa-arrow-up"></i>
                        <span class="widget-caption">EVALUACIÓN</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                             <!--a href="#" data-toggle="dispose">
                                <i class="fa fa-times"></i>
                             </a-->
                        </div>
                    </div>
                    <?php
                    $cargo_id = $this->session->userdata('user.cargo_id');
                    switch ($cargo_id) {
                        case 2:  ?>
                            <a href="<?php echo base_url('evaluacion_director') ?>">
                    <?php
                            break;
                        case 3:
                    ?>
                            <a href="<?php echo base_url('evaluacion_administrador') ?>">
                    <?php
                             break;
                        case 6:
                    ?>
                            <a href="<?php echo base_url('evaluacion_defensor') ?>">
                    <?php
                            break;
                    }
                    ?>

                    <div class="widget-body">
                        Acceder

                    </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="widget">
                    <div class="widget-header bordered-top bordered-pink">
                        <i class="widget-icon fa fa-arrow-up"></i>
                        <span class="widget-caption">REPORTES</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                            <!--a href="#" data-toggle="dispose">
                               <i class="fa fa-times"></i>
                            </a-->
                        </div>
                    </div>
                    <a href="<?php echo base_url('404_override') ?>">
                    <div class="widget-body">
                        Acceder
                    </div>
                    </a>
                </div>
            </div>
            <?php if($this->session->userdata('rol.id')==1): ?>

            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="widget">
                    <div class="widget-header bordered-top bordered-pink">
                        <i class="widget-icon fa fa-arrow-up"></i>
                        <span class="widget-caption">PERSONAL</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                            <!--a href="#" data-toggle="dispose">
                               <i class="fa fa-times"></i>
                            </a-->
                        </div>
                    </div>
                    <a href="<?php echo base_url('personal') ?>">
                        <div class="widget-body">
                            Acceder
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="widget">
                    <div class="widget-header bordered-top bordered-pink">
                        <i class="widget-icon fa fa-arrow-up"></i>
                        <span class="widget-caption">USUARIO</span>
                        <div class="widget-buttons">
                            <a href="#" data-toggle="maximize">
                                <i class="fa fa-expand"></i>
                            </a>
                            <a href="#" data-toggle="collapse">
                                <i class="fa fa-minus"></i>
                            </a>
                            <!--a href="#" data-toggle="dispose">
                                <i class="fa fa-times"></i>
                            </a-->
                        </div>
                    </div>
                    <a href="<?php echo base_url('usuarios') ?>">
                        <div class="widget-body">
                            Acceder
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
    <!-- /Page Body -->
</div>