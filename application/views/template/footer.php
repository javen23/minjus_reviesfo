                </div>
        </div>
        <!-- Javascript Libraries -->
        <script src="<?php echo base_url()?>public_html/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>public_html/js/slimscroll/jquery.slimscroll.min.js"></script>
        <!--Scripts-->
        <script src="<?php echo base_url()?>public_html/js/beyond.js"></script>
        <script src="<?php echo base_url()?>public_html/js/toastr/toastr.js"></script>
        <?php if(isset($scripts) && count($scripts)>0): ?>
        <?php foreach($scripts as $script):?>
            <script src="<?php echo base_url($script.'.js') ?>"></script>
        <?php endforeach;?>
        <?php endif;?>
        <script src="<?php echo base_url('public_html/js/global.js') ?>"></script>
    </body>
</html>