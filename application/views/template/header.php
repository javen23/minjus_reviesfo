<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo (isset($titulo) && $titulo !== "") ? $titulo . " | " : ""; echo NAME_PROYECT ?></title>

        <!-- Vendor CSS -->
        <link href="<?php echo base_url()?>public_html/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url()?>public_html/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url()?>public_html/css/beyond.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>public_html/css/demo.min.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>public_html/css/animate.min.css" rel="stylesheet"/>
        <link href="<?php echo base_url()?>public_html/css/typicons.min.css" rel="stylesheet" />
        <script src="<?php echo base_url()?>public_html/js/skins.min.js"></script>

        <?php if(isset($styles) && count($styles)>0): ?>
        <?php foreach($styles as $style):?>
            <link href="<?php echo base_url($style.'.css') ?>" rel="stylesheet">
        <?php endforeach;?>
        <?php endif;?>
        <link href="<?php echo base_url()?>public_html/css/styles.css" rel="stylesheet">

        <script type="text/javascript">
        var CI = {
            'base_url' : '<?php echo base_url(); ?>',
            'site_url' : '<?php echo site_url(); ?>'
        };
        </script>
        <script src="<?php echo base_url()?>public_html/js/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).on('ready', function () {
                var url         = window.location;
                var ruta_main   = $('#sidebar a[href="'+url+'"]');
                ruta_main.parent().addClass('active');
                $('#sidebar a').filter(function () {
                    return this.href === url;
                }).parent().addClass('active');
            });
        </script>
    </head>
    <body>
        <!-- Loading Container -->
        <div class="loading-container">
            <div class="loader"></div>
        </div>
        <!-- Navbar -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="navbar-container">
                    <!-- Navbar Barnd -->
                    <div class="navbar-header pull-left">
                        <a href="<?php echo base_url()?>" class="navbar-brand">
                            <small style="line-height: 40px;">
                                <?php echo NAME_PROYECT ?>
<!--                                <img src="--><?php //echo base_url()?><!--public_html/img/logo.png" alt="" />-->
                            </small>
                        </a>
                    </div>
                    <!-- /Navbar Barnd -->
                    <!-- Sidebar Collapse -->
                    <div class="sidebar-collapse" id="sidebar-collapse">
                        <i class="collapse-icon fa fa-bars"></i>
                    </div>
                    <!-- /Sidebar Collapse -->
                    <!-- Account Area and Settings --->
                    <div class="navbar-header pull-right">
                        <div class="navbar-account">
                            <ul class="account-area">
                                <li>
                                    <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                        <div class="avatar" title="View your public profile">
                                            <img src="<?php echo base_url()?>public_html/img/avatars/user.jpg">
                                        </div>
                                        <section>
                                            <h2><span class="profile"><span><?php echo $this->session->userdata('user.apellidos')?></span></span></h2>
                                        </section>
                                    </a>
                                    <!--Login Area Dropdown-->
                                    <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
<!--                                        <li class="username"><a>--><?php //echo $this->session->userdata('user.napellido')?><!--</a></li>-->
                                        <li class="email"><a><?php echo $this->session->userdata('user.email')?></a></li>
<!--                                        Avatar Area-->
                                        <li>
                                            <div class="avatar-area">
                                                <img src="<?php echo base_url()?>public_html/img/avatars/user.jpg" class="avatar">
<!--                                                <span class="caption">Change Photo</span>-->
                                            </div>
                                        </li>
<!--                                        Avatar Area-->
<!--                                        <li class="edit">-->
<!--                                            <a href="profile.html" class="pull-left">Profile</a>-->
<!--                                            <a href="#" class="pull-right">Setting</a>-->
<!--                                        </li>-->
                                        <!--Theme Selector Area-->
<!--                                        <li class="theme-area">-->
<!--                                            <ul class="colorpicker" id="skin-changer">-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#5DB2FF;" rel="--><?php //echo base_url()?><!--public_html/css/skins/blue.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#2dc3e8;" rel="--><?php //echo base_url()?><!--public_html/css/skins/azure.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#03B3B2;" rel="--><?php //echo base_url()?><!--public_html/css/skins/teal.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#53a93f;" rel="--><?php //echo base_url()?><!--public_html/css/skins/green.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#FF8F32;" rel="--><?php //echo base_url()?><!--public_html/css/skins/orange.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#cc324b;" rel="--><?php //echo base_url()?><!--public_html/css/skins/pink.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#AC193D;" rel="--><?php //echo base_url()?><!--public_html/css/skins/darkred.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#8C0095;" rel="--><?php //echo base_url()?><!--public_html/css/skins/purple.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#0072C6;" rel="--><?php //echo base_url()?><!--public_html/css/skins/darkblue.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#585858;" rel="--><?php //echo base_url()?><!--public_html/css/skins/gray.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#474544;" rel="--><?php //echo base_url()?><!--public_html/css/skins/black.min.css"></a></li>-->
<!--                                                <li><a class="colorpick-btn" href="#" style="background-color:#001940;" rel="--><?php //echo base_url()?><!--public_html/css/skins/deepblue.min.css"></a></li>-->
<!--                                            </ul>-->
<!--                                        </li>-->
                                        <!--/Theme Selector Area-->
                                        <li class="dropdown-footer">
                                            <a href="<?php echo base_url('logout')?>">
                                                Cerrar sesión
                                            </a>
                                        </li>
                                    </ul>
                                    <!--/Login Area Dropdown-->
                                </li>
                                <!-- /Account Area -->
                                <!--Note: notice that setting div must start right after account area list.
                                no space must be between these elements-->
                                <!-- Settings -->
                            </ul><div class="setting">
                                <a id="btn-setting" title="Setting" href="#">
                                    <i class="icon glyphicon glyphicon-cog"></i>
                                </a>
                            </div><div class="setting-container">
                                <label>
                                    <input type="checkbox" id="checkbox_fixednavbar">
                                    <span class="text">Fixed Navbar</span>
                                </label>
                                <label>
                                    <input type="checkbox" id="checkbox_fixedsidebar">
                                    <span class="text">Fixed SideBar</span>
                                </label>
                                <label>
                                    <input type="checkbox" id="checkbox_fixedbreadcrumbs">
                                    <span class="text">Fixed BreadCrumbs</span>
                                </label>
                                <label>
                                    <input type="checkbox" id="checkbox_fixedheader">
                                    <span class="text">Fixed Header</span>
                                </label>
                            </div>
                            <!-- Settings -->
                        </div>
                    </div>
                    <!-- /Account Area and Settings -->
                </div>
            </div>
        </div>
        <!-- /Navbar -->
        <!-- Main Container -->
        <div class="main-container container-fluid">
            <!-- Page Container -->
            <div class="page-container">