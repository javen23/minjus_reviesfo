<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->
<!--    <div class="sidebar-header-wrapper">-->
<!--        <input type="text" class="searchinput" />-->
<!--        <i class="searchicon fa fa-search"></i>-->
<!--        <div class="searchhelper">Search Reports, Charts, Emails or Notifications</div>-->
<!--    </div>-->
    <!-- /Page Sidebar Header -->
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu">
        <!--Dashboard-->
        <li>
            <a href="<?php echo base_url()?>">
                <i class="menu-icon glyphicon glyphicon-home"></i>
                <span class="menu-text"> Inicio </span>
            </a>
        </li>
        <li>
        <?php
            $cargo_id = $this->session->userdata('user.cargo_id');
            switch ($cargo_id) {
            case 2:
        ?>
                <a href="<?php echo base_url('evaluacion_director') ?>">
        <?php
                break;
            case 3:
        ?>
                <a href="<?php echo base_url('evaluacion_administrador') ?>">
        <?php
                break;
            case 6:
        ?>
                <a href="<?php echo base_url('evaluacion_defensor') ?>">
        <?php
                break;
        }
        ?>
                <i class="menu-icon fa fa-file-word-o"></i>
                <span class="menu-text"> Evaluación </span>
            </a>
        </li>

        <li>
            <a href="<?php echo base_url('404_override')?>">
                <i class="menu-icon fa fa-server"></i>
                <span class="menu-text"> Reportes </span>
            </a>
        </li>
        <!--Databoxes-->
        <?php if($this->session->userdata('rol.id')==1): ?>
        <li>
            <a href="<?php echo base_url('personal')?>">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text"> Personal </span>
            </a>
        </li>

        <li>
            <a href="<?php echo base_url('usuarios')?>">
                <i class="menu-icon typcn typcn-user"></i>
                <span class="menu-text"> Usuarios </span>
            </a>
        </li>
        <?php endif;?>
    </ul>
    <!-- /Sidebar Menu -->
</div>