<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('nombre_sede')){

    function nombre_sede($nombre_sede){

        if(is_null($nombre_sede)){
            $nombre_sede    = "---";
        }else{
            $nombre_sede    = explode("_", $nombre_sede);
            $nombre_sede    = strtoupper($nombre_sede[0]).'<br>'.strtoupper($nombre_sede[1]);
        }
        return $nombre_sede;

    }

}

if (!function_exists('verifica_hora')) {

    function verifica_hora($horaInicio) {
        $inicioValores  = explode(":", $horaInicio);
        $inicioMinutos  = $inicioValores[0] * 60 + $inicioValores[1];
        $finValores     = explode(":", '03:59:59');
        $finMinutos     = $finValores[0] * 60 + $finValores[1];
        if ($horaInicio == '00:00:00') {
            $res        = 'no_marco';
        } else if ($inicioMinutos < $finMinutos) {
            $res        = 'paso_hora';
        } else {
            $res        = '';
        }
        return $res;
    }

}

if(!function_exists('encodeUriComponent')){

    function encodeUriComponent($texto){
        return rawurlencode($texto);
    }

}

if (!function_exists('verificar_estado')) {

    function verificar_estado($estado) {
        if ($estado == 1) :
            $res = "<i class='fa fa-circle'></i> ";
        else:
            $res = "<i class='fa fa-circle-o'></i>";
        endif;
        return $res;
    }

}

if (!function_exists('assets')) {

    function assets($path) {
        if (substr($path, 0, 2) === '//') {
            $ruta_path = $path;
        } else {
            $ruta_path = base_url() . 'public/' . $path;
        }
        return $ruta_path;
    }

}


if (!function_exists('no_tilde')) {

    function no_tilde($nombre) {
        $find           = array('Ã', 'Ã‰', 'Ã', 'Ã“', 'Ãš', 'Ã‘', 'Ã¡', 'Ã©', 'Ã­', 'Ã³', 'Ãº', 'Ã±');
        $repl           = array('A', 'E', 'I', 'O', 'U', 'N', 'a', 'e', 'i', 'o', 'u', 'n');
        $nombreLimpio   = str_replace($find, $repl, $nombre);
        return $nombreLimpio;
    }

}

if (!function_exists('tilde_mayuscula')) {

    function tilde_mayuscula($nombre) {
        $find           = array('Ã¡', 'Ã©', 'Ã­', 'Ã³', 'Ãº', 'Ã±');
        $repl           = array('Ã', 'Ã‰', 'Ã', 'Ã“', 'Ãš', 'Ã‘');
        $nombreLimpio   = str_replace($find, $repl, $nombre);
        return $nombreLimpio;
    }

}

if (!function_exists('zeros_left')) {

    function zeros_left($valor, $longitud) {
        return str_pad($valor, $longitud, '0', STR_PAD_LEFT);
    }

}

if(!function_exists('url_amigable')){

    function url_amigable($url) {
      $urlMinuscula     = strtolower($url);
      $buscarLetra      = array('Ã¡', 'Ã©', 'Ã­', 'Ã³', 'Ãº', 'Ã±');
      $reemplazar       = array('a', 'e', 'i', 'o', 'u', 'n');
      $urlNueva         = str_replace($buscarLetra, $reemplazar, $urlMinuscula);
      $buscarCaracteres = array(' ', '&', '\r\n', '\n', '+');
      $urlAceptable     = str_replace($buscarCaracteres, '-', $urlNueva);
      $find             = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
      $repl             = array('', '-', '');
      $urlRetornar      = preg_replace($find, $repl, $urlAceptable);
      return $urlRetornar;
    }
}