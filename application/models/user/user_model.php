<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('security');
        $this->idLogin      = $this->session->userdata('user.id');
        $this->nameUsuario  = $this->session->userdata('user.name');
   }

    public function all_users($id_=NULL){
        if(!is_null($id_)){
            $sql    = $this->select_user_rol();
            $sql    .= " WHERE usu.id={$id_}";
            $query  = $this->db->query($sql);
            if($query->num_rows()==1){
                return $query->row_array();
            }
        }
        $sql    = $this->select_user_rol();
        $query  = $this->db->query($sql);
        return $query->result_array();
   }

    public function all_rols() {
        $query = $this->db->select('id,nombre')->get('tbl_rol');
        if($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;
    }

    public function actualizar_accion($params){
        $estado = ($params['estado']==1) ? 0: 1;
        $datos  = array(
            'estado'          => $estado,
            'modificado_por'  => $this->nameUsuario,
            'fecha_modificado'=> date("Y-m-d H:i:s")
        );
      $this->db->where('id',$params['id']);
      return $this->db->update('tbl_usuario',$datos);
    }
    
    private function select_user_rol(){
        return "SELECT
                    usu.*,
                    rol.id AS rol_id,
                    rol.nombre AS rol,
                    persona.nombres as nombres,
                    persona.apellido_paterno as apellido_paterno,
                    persona.apellido_materno as apellido_materno,
                    persona.email as email
                FROM
                    tbl_usuario AS usu
                    LEFT JOIN tbl_rol AS rol ON usu.tbl_rol_id=rol.id
                    INNER JOIN tbl_persona as persona ON usu.tbl_persona_id=persona.id
                    ";
    }

    public function insertar_usuario($params){
        $datos = array(
            'tbl_rol_id'    => $params['rol'],
            'usuario'       => strtolower($params['usuario']),
            'password'      => do_hash($params['password']),
            'email'         => strtolower($params['email']),
            'nombres'       => strtoupper($params['nombres']),
            'apellidos'     => strtoupper($params['apellidos']),
            'creado_por'    => $this->nameUsuario,
            'fecha_creado'  => date("Y-m-d H:i:s")
        );
        return $this->db->insert('tbl_usuario',$datos);
    }

    public function actualizar_usuario($params){
        $datos = array(
            'usuario'           => strtolower($params['usuario']),
            'nombres'           => strtoupper($params['nombres']),
            'apellidos'         => strtoupper($params['apellidos']),
            'email'             => strtolower($params['email']),
            'modificado_por'    => $this->nameUsuario,
            'fecha_modificado'  => date("Y-m-d H:i:s")
        );
        if(isset($params['rol'])){
            $this->db->set('tbl_rol_id', $params['rol']);
        }

        if($params['password'] != ''){
            $this->db->set('password', do_hash($params['password']));
        }

        $this->db->where('id', $params['hidden_id_user']);
        return $this->db->update('tbl_usuario', $datos);
    }

    public function verificar_data($params) {
        if(isset($params['user_crear']))
        {
            $query = $this->db->where('nombre', $params['user_crear'])->select('nombre')->get('tbl_usuario');
        }
        elseif (isset($params['email']))
        {
            $query = $this->db->where('email',$params['email'])->select('email')->get('tbl_usuario');
        }
        if($query->num_rows()==0)
        {
            return TRUE;
        }
        return FALSE;
    }
    
}
