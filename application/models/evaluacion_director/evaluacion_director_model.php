<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluacion_Director_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('security');
        $this->idLogin      = $this->session->userdata('user.id');
        $this->nameUsuario  = $this->session->userdata('user.name');
   }

   /*Obtenemos cargo, direccion_distrital, nombre para Cabecera Ficha*/
   public function obtener_datos_personal($perona_id) {
       $sql    = " Select persona.*,
                           cargo.id as cargo_id, 
                           cargo.descripcion as cargo,
                            distrito_judicial.id as distrito_judicial_id,
                           distrito_judicial.descripcion as distrito_judicial                                   
                    from tbl_persona as persona
                    INNER JOIN tbl_cargo as cargo ON persona.tbl_cargo_id = cargo.id
                    INNER JOIN tbl_distrito_judicial as distrito_judicial ON persona.tbl_distrito_judicial_id = distrito_judicial.id
                    WHERE persona.id = '".$perona_id."'";

       $query  = $this->db->query($sql);
       return $query->row_array();
   }

   public function obtener_evaluacion_titulo($cargo_id) {
        $sql = "select  titulo.id as titulo_id,
                        titulo.descripcion as titulo_descripicion,
                        evaluacion.descripcion as evaluacion_descripcion
                from tbl_evaluacion_titulo as titulo
                INNER JOIN tbl_evaluacion as evaluacion ON evaluacion.id = titulo.tbl_evaluacion_id
                where tbl_cargo_id = ".$cargo_id;

       $query  = $this->db->query($sql);
       return $query->result_array();
   }

    public function obtener_evaluacion_detalle($cargo_id) {
        $sql = "select
                detalle.id as detalle_id,
                detalle.descripcion as detalle_descripcion,
                detalle.descripcion_informativo as detalle_informativo,
                titulo.id as titulo_id
                from
                  tbl_evaluacion_detalle as detalle
                  INNER JOIN tbl_evaluacion_titulo as titulo ON detalle.tbl_evaluacion_titulo_id = titulo.id
                  INNER JOIN tbl_evaluacion as evaluacion ON evaluacion.id = titulo.tbl_evaluacion_id
                where tbl_cargo_id = ".$cargo_id;

        $query  = $this->db->query($sql);
        return $query->result_array();
    }









    public function todo_personal($id_=NULL){
        if(!is_null($id_)){
            $sql    = $this->select_personal();
            $sql    .= " WHERE persona.id={$id_}";
            $query  = $this->db->query($sql);
            if($query->num_rows()==1){
                return $query->row_array();
            }
        }
        $sql    = $this->select_personal();
        $query  = $this->db->query($sql);
        return $query->result_array();
   }

    public function all_rols() {
        $query = $this->db->select('id,nombre')->get('tbl_rol');
        if($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;
    }

    public function actualizar_accion($params){
        $estado = ($params['estado']==1) ? 0: 1;
        $datos  = array(
            'estado'          => $estado,
            'modificado_por'  => $this->nameUsuario,
            'fecha_modificado'=> date("Y-m-d H:i:s")
        );
      $this->db->where('id',$params['id']);
      return $this->db->update('tbl_usuario',$datos);
    }
    
    private function select_personal(){
        return "SELECT
                    persona.*,
                    cargo.id as cargo_id,
                    cargo.descripcion AS cargo_descripcion,
                    distrito.id as distrito_id, 
                    distrito.descripcion as distrito_descripcion,
                    sede.id as sede_id,
                    sede.descripcion as sede_descripcion
                FROM
                    tbl_persona AS persona
                    LEFT JOIN tbl_cargo AS cargo ON persona.tbl_cargo_id=cargo.id
                    INNER JOIN tbl_distrito_judicial as distrito ON persona.tbl_distrito_judicial_id = distrito.id
                    INNER JOIN tbl_sede as sede ON persona.tbl_sede_id = sede.id
                    ";
    }

    public function insertar_usuario($params){
        $datos = array(
            'tbl_rol_id'    => $params['rol'],
            'usuario'       => strtolower($params['usuario']),
            'password'      => do_hash($params['password']),
            'email'         => strtolower($params['email']),
            'nombres'       => strtoupper($params['nombres']),
            'apellidos'     => strtoupper($params['apellidos']),
            'creado_por'    => $this->nameUsuario,
            'fecha_creado'  => date("Y-m-d H:i:s")
        );
        return $this->db->insert('tbl_usuario',$datos);
    }

    public function actualizar_usuario($params){
        $datos = array(
            'usuario'           => strtolower($params['usuario']),
            'nombres'           => strtoupper($params['nombres']),
            'apellidos'         => strtoupper($params['apellidos']),
            'email'             => strtolower($params['email']),
            'modificado_por'    => $this->nameUsuario,
            'fecha_modificado'  => date("Y-m-d H:i:s")
        );
        if(isset($params['rol'])){
            $this->db->set('tbl_rol_id', $params['rol']);
        }

        if($params['password'] != ''){
            $this->db->set('password', do_hash($params['password']));
        }

        $this->db->where('id', $params['hidden_id_user']);
        return $this->db->update('tbl_usuario', $datos);
    }

    public function verificar_data($params) {
        if(isset($params['user_crear']))
        {
            $query = $this->db->where('nombre', $params['user_crear'])->select('nombre')->get('tbl_usuario');
        }
        elseif (isset($params['email']))
        {
            $query = $this->db->where('email',$params['email'])->select('email')->get('tbl_usuario');
        }
        if($query->num_rows()==0)
        {
            return TRUE;
        }
        return FALSE;
    }
    
}
