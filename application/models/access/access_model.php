<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function logearme($user, $pass) {
        $sql = "SELECT
                    usu.*,
                    rol.nombre AS nombre_rol,                                                                               
                    persona.nombres as nombres,
                    persona.apellido_paterno as apellido_paterno,
                    persona.apellido_materno as apellido_materno,
                    persona.email as email,
                    cargo.id as cargo_id,
                    cargo.descripcion as cargo_descripcion
                FROM
                    tbl_usuario AS usu
                    LEFT JOIN tbl_rol AS rol ON usu.tbl_rol_id=rol.id
                    INNER JOIN tbl_persona persona ON usu.tbl_persona_id = persona.id
                    INNER JOIN tbl_cargo cargo ON persona.tbl_cargo_id = cargo.id
                WHERE 
                    usu.usuario='".$user."'
                    AND usu.clave='".sha1($pass)."'
                    AND usu.estado=1;";
        log_message('INFO', "<<<============ Logearme {$sql}============>>>");
        $query  = $this->db->query($sql);
        if($query->num_rows()==1) {
            return $query->row_array();
        }
        return NULL;
    }
}