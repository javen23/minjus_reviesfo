<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_controller extends CI_Controller
{

    public $modelAccess;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('access/access_model', 'modelAccess');
    }

    public function login()
    {
        if (!$this->session->userdata('user.id'))
        {
            $user   = $this->security->xss_clean(strip_tags($this->input->post('sendUser', TRUE)));
            $pass   = $this->security->xss_clean(strip_tags($this->input->post('sendPass', TRUE)));
            if ($user && $pass)
            {
                $response   = $this->modelAccess->logearme($user, $pass);
                if(!is_null($response))
                {
                    //$name_ = explode(" ", $response['nombres']);
                    //$apel_ = explode(" ", $response['apellidos']);
                    $dato_usuario = array(
                        'user.id'       => $response['id'],
                        'user.cargo_id' => $response['cargo_id'],
                        'user.persona_id' => $response['tbl_persona_id'],
                        'user.name'     => $response['usuario'],
                        'user.nombres'  => $response['nombres'],
                        'user.apellidos'=> $response['apellido_paterno']. " ".$response['apellido_materno'],
                        'user.email'    => $response['email'],
                        'rol.id'        => $response['tbl_rol_id'],
                        'rol.name'      => $response['nombre_rol']
                    );
                    $this->session->set_userdata($dato_usuario);
                    redirect('', 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('usuario_incorrecto', 'Datos ingresados incorrectos...');
                    redirect('login', 'refresh');
                }
            }
            else
            {
                $data['titulo']     = "ACCESO AL SISTEMA";
                $data['contenido']  = "access/login";
                $this->load->view('template', $data);
            }
        }
        else
        {
            redirect('', 'refresh');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user.id');
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */