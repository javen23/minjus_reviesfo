<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluacion_Defensor_controller extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('evaluacion_defensor/evaluacion_defensor_model', 'modelEvaluacionDefensor');
        $this->load->helper('string');
        $this->load->helper('security');
        /* Valida que el rol 1 pueda visaulizar la pagina
        $this->id_rol   = $this->session->userdata('rol.id');
        $array_valores  = array(1);
        if (!in_array($this->id_rol,$array_valores))
        {
            redirect('', "refresh");
        }
        */
        // if ($this->id_rol != 1) {
        //     redirect('', "refresh");
        // }
    }


   public function index()
   {
        $datos['styles']      = array('public_html/css/dataTables.bootstrap');
        $datos['scripts']     = array('public_html/js/datatable/jquery.dataTables.min',
                                        'public_html/js/datatable/ZeroClipboard',
                                        'public_html/js/datatable/dataTables.tableTools.min',
                                        'public_html/js/datatable/dataTables.bootstrap.min',
                                        'public_html/js/jquery-validation/dist/jquery.validate',
                                        'public_html/js/usuario/user');
        //$datos['all_personal']   = $this->modelEvaluacion->todas_las_evaluaciones();
        /*Obtenermos los datos del personal para la cabecera Ficha */
        $persona_id = $this->session->userdata('user.persona_id');
        $datos['personal']    = $this->modelEvaluacionDefensor->obtener_datos_personal($persona_id);

        /* Obtenemos el contenido de la Evaluación por cargo */
        $cargo_id = $datos['personal']['cargo_id'];
        $datos['evaluacion_titulo'] = $this->modelEvaluacionDefensor->obtener_evaluacion_titulo($cargo_id);
        $datos['evaluacion_detalle'] = $this->modelEvaluacionDefensor->obtener_evaluacion_detalle($cargo_id);
        $datos['titulo']      = "Listado de Evaluación de los Lineamientos de Trabajo  - INSCRIPCIÓN ITINERANTE ";
        $datos['contenido']   = "evaluacion/create";
        $this->load->view('template', $datos);
   }












    public function lists() {
        $datos['all_users']    = $this->modelUser->all_users();
        $this->load->view('user/list_ajax', $datos);
    }

   public function create() {
         if ($this->input->post('dataUser')==='createUser')
         {
            $_REQUEST['rol']        = $this->security->xss_clean($this->input->post('rol'));
            $_REQUEST['nombres']    = $this->security->xss_clean(strip_tags(trim($this->input->post('nombres'))));
            $_REQUEST['apellidos']  = $this->security->xss_clean(strip_tags(trim($this->input->post('apellidos'))));
            $_REQUEST['email']      = $this->security->xss_clean(strip_tags(trim($this->input->post('email'))));
            $_REQUEST['password']   = $this->security->xss_clean(strip_tags(trim($this->input->post('password'))));
            $p_last                 = explode(" ",$_REQUEST['apellidos']);
            $_REQUEST['usuario']    = $_REQUEST['nombres'][0].$p_last[0];
            $status                 = $this->modelUser->insertar_usuario($_REQUEST);
            $this->estado_accion_resultado($status, 'CREADO');
         }
         else
         {
            if ($this->input->is_ajax_request())
            {
                $datos['all_rols'] = $this->modelUser->all_rols();
                $this->load->view('user/create', $datos);
            }
        }
   }

    private function estado_accion_resultado($status, $accion){
        if ($status) {
            log_message('INFO', "<============ Usuario {$accion} satisfactoriamente ============>");
            $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_output(json_encode(array('mensaje'=>"Usuario {$accion} correctamente",'tipo'=>'success','icono'=>'fa-smile-o')));
        } else {
            log_message('ERROR', $status . "Usuario NO pudo ser {$accion}");
            $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_output(json_encode(array('mensaje'=>"Usuario NO fue {$accion}",'tipo'=>'warning','icono'=>'fa-frown-o')));
        }
    }
//
   public function edit() {
        if ($this->input->post('dataUser') == 'EditUser') {
            $_REQUEST['rol']        = $this->security->xss_clean($this->input->post('rol'));
            $_REQUEST['nombres']    = $this->security->xss_clean(strip_tags(trim($this->input->post('nombres'))));
            $_REQUEST['apellidos']  = $this->security->xss_clean(strip_tags(trim($this->input->post('apellidos'))));
            $_REQUEST['email']      = $this->security->xss_clean(strip_tags(trim($this->input->post('email'))));
            $_REQUEST['password']   = $this->security->xss_clean(strip_tags(trim($this->input->post('password'))));
            $p_last                 = explode(" ",$_REQUEST['apellidos']);
            $_REQUEST['usuario']    = $_REQUEST['nombres'][0].$p_last[0];
            $status                 = $this->modelUser->actualizar_usuario($_REQUEST);
            $this->estado_accion_resultado($status, 'EDITADO');
        }else{
            if ($this->input->is_ajax_request()) {
                $id_user            = $this->input->get('id_user');
                $datos['all_rols']  = $this->modelUser->all_rols();
                $datos['user']      = $this->modelUser->all_users($id_user);
                $this->load->view('user/edit', $datos);
            }
        }
   }
//
    public function verifica() {
        if(isset($_REQUEST['user_crear'])){
            $_REQUEST['user_crear'] = $this->input->post('user_crear');
        }elseif (isset($_REQUEST['email'])){
            $_REQUEST['email'] = $this->input->post('email');
        }
        $user = $this->modelUser->verificar_data($_REQUEST);
        if ($user) {
            echo "true";
        }else{
            echo "false";
        }
    }
//

   public function state() {
       if ($this->input->is_ajax_request()) {
           $status = $this->modelUser->actualizar_accion($this->input->post());
           $this->accion_actualizado($status, $this->input->post('estado'));
       }

   }

    private function accion_actualizado($status, $key) {
        if ($status)
        {
            if($key==1)
            {
                $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_output(json_encode(array('mensaje'=>"Usuario ya no podrá acceder al sistema",'tipo'=>'danger','icono'=>'fa-frown-o')));
            }
            else
            {
                $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_output(json_encode(array('mensaje'=>"Se reestablecio el usuario al sistema",'tipo'=>'success','icono'=>'fa-smile-o')));

            }
        }
        else
        {
            $this->output
                ->set_content_type('application/json;charset=utf-8')
                ->set_output(json_encode(array('mensaje'=>'Se produjo un error al cambiar el estado', 'tipo'=>'warning')));
        }
    }
    
}
