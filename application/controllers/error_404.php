<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->output->set_status_header('404');
        $datos['titulo']    = "Error 404";
        $datos['contenido'] = "errors/personalized/error_404";
        $this->load->view('template', $datos);
    }

    public function show_error() {
        $this->output->set_status_header('404');
        $datos['titulo']    = "Error 404";
        $datos['contenido'] = "errors/personalized/error_404";
        $this->load->view('template', $datos);
    }

}
