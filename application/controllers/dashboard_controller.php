<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
//        $datos['styles']    = array("");
//        $datos['scripts']   = array("");
        $datos['titulo']    = "Dashboard";
        $datos['contenido'] = "dashboard/portada";
        $this->load->view('template', $datos);
    } 
}
