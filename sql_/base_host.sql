
CREATE TABLE `tbl_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `creado_por` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'root',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO tbl_rol(nombre) VALUES ('SUPER ADMIN');
INSERT INTO tbl_rol(nombre) VALUES ('ADMINISTRADOR');
INSERT INTO tbl_rol(nombre) VALUES ('EVALUADOR');
INSERT INTO tbl_rol(nombre) VALUES ('EDITOR');
INSERT INTO tbl_rol(nombre) VALUES ('CONSULTA');


CREATE TABLE `tbl_cargo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `creado_por` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'root',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_cargo(descripcion) values('INFORMATICO');
insert into tbl_cargo(descripcion) values('DIRECTOR');
insert into tbl_cargo(descripcion) values('ADMINISTRADOR');
insert into tbl_cargo(descripcion) values('ASISTENTE ADMINISTRATIVO');
insert into tbl_cargo(descripcion) values('AUXILIAR ADMINISTRATIVO');
insert into tbl_cargo(descripcion) values('DEFENSOR PUBLICO');
insert into tbl_cargo(descripcion) values('CONCILIADORA');
insert into tbl_cargo(descripcion) values('MEDICO LEGISTA');
insert into tbl_cargo(descripcion) values('PERITO CRIMINALISTICA');
insert into tbl_cargo(descripcion) values('TRABAJADORA SOCIAL');
insert into tbl_cargo(descripcion) values('MOTORIZADO');
insert into tbl_cargo(descripcion) values('CHOFER');


CREATE TABLE `tbl_distrito_judicial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(80) NOT NULL,
  `creado_por` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'root',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('AMAZONAS');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('ANCASH');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('APURIMAC');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('AREQUIPA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('AYACUCHO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('CAJAMARCA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('CALLAO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('ANCASH');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('CAÑETE');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('CUSCO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('ANCASH');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('HUANCAVELICA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('HUANUCO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('HUAURA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('ICA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('JUNIN');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LA LIBERTAD');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LAMBAYEQUE');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA ESTE');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA NOR OESTE - VENTANILLA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA NORTE');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA SUR');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LIMA NORTE');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('LORETO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('MADRE DE DIOS');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('MOQUEGUA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('PASCO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('PIURA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('PUNO');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('SAN MARTIN');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('SANTA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('SULLANA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('TACNA');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('TUMBES');
INSERT INTO `tbl_distrito_judicial` (`descripcion`) VALUES ('UCAYALI');


CREATE TABLE `tbl_sede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_distrito_judicial_id` int(11) NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `creado_por` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'root',
  `fecha_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'ALEGRA AYNA','AV. HUANTA S/N - SAN FRANCISCO');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'CANGALLO','JIRON MARIA PARADO DE BELLIDO Mz. C-1 Lte. 25 ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'CENTRAL','AV. 26 DE ENERO N° 407 - 408');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'CHURCAMPA','JIRÓN  SAN ANTONIO N°182 ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'CORA CORA','JIRÓN 2 DE MAYO N° 907 ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'HUANCASANCOS','JIRON 28 DE JULIO S/N (CENTRO POBLADO DE HUANCA SANCOS Mz. P Lte. 4)');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'HUANTA','JIRON RAZUHUILLCA N° 318, MZ. D LOTE 25 – CERCADO DE HUANTA');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'KIMBIRI','AVENIDA AREOPUERTO S/N ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'MEGA ALEGRA HUAMANGA','AV. 26 DE ENERO N° 401');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'OCAÑA','PLAZA PRINCIPAL S/N');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'PAUZA','AVENIDA APOSTOL SANTIAGO N° 685 ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'PICHARI','JIRÓN MANUEL A. ODRIA MZ. E-I, LOTE 18, URBANIZACIÓN MARAVILLA');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'PUQUIO','JIRÓN 9 DE DICIEMBRE S/N ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'SAN MIGUEL','URBANIZACIÓN MARISCAL CACERES S/N - CENTRO POBLADO SAN MIGUEL MZ. G1, LOTE 5');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'SUCRE','JIRON BOLÍVAR MZ. D, LOTE. 6A');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'VICTOR FAJARDO','JIRÓN FRANCIA S/N (CENTRO POBLADO HUANCAPI MZ. T LOTE N° 16 – SECTOR SAN LUIS) ');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'VILCASHUAMAN','AVENIDA MARIA PARADO DE BELLIDO S/N');
insert into tbl_sede(tbl_distrito_judicial_id,descripcion,direccion) values(5,'VINCHOS','AVENIDA JOSE MARÍA GAMBOA MZ. E – LOTE 19');


/*  tbl_persona */
CREATE TABLE `tbl_persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_cargo_id` int(11) NOT NULL,
  `tbl_distrito_judicial_id` int(10) NOT NULL,
  `tbl_sede_id` int(10) NOT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `apellido_paterno` varchar(100) DEFAULT NULL,
  `apellido_materno` varchar(100) DEFAULT NULL,
  `nombres_cadena` varchar(200) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `telefono_minjus` varchar(10) DEFAULT NULL,
  `telefono_personal` varchar(10) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `tbl_persona` (`tbl_cargo_id`, `tbl_distrito_judicial_id`, `tbl_sede_id`, `nombres`, `apellido_paterno`, `apellido_materno`,email) VALUES(1,5,3,'JUAN ALEXIS','ESCALANTE','ARRUNATEGUI','jalexis@minjus.gob.pe');  /*Informatico*/
INSERT INTO `tbl_persona` (`tbl_cargo_id`, `tbl_distrito_judicial_id`, `tbl_sede_id`, `nombres`, `apellido_paterno`, `apellido_materno`,email) VALUES(2,5,3,'ROLANDO','ZAMBRANO','PAZ','rzambrano@minjus.gob.pe'); /*Director*/
INSERT INTO `tbl_persona` (`tbl_cargo_id`, `tbl_distrito_judicial_id`, `tbl_sede_id`, `nombres`, `apellido_paterno`, `apellido_materno`,email) VALUES(3,5,3,'CESAR','PARIONA','GONZALES','cpariona@minjus.gob.pe'); /*Administrador*/
INSERT INTO `tbl_persona` (`tbl_cargo_id`, `tbl_distrito_judicial_id`, `tbl_sede_id`, `nombres`, `apellido_paterno`, `apellido_materno`,email) VALUES(6,5,3,'JACKELINE','ZAVALA','TORRES','jzavala@minjus.gob.pe'); /*Defensor Evaluador*/
INSERT INTO `tbl_persona` (`tbl_cargo_id`, `tbl_distrito_judicial_id`, `tbl_sede_id`, `nombres`, `apellido_paterno`, `apellido_materno`,email) VALUES(6,5,3,'ROSA','PEREZ','PAZ','rperez@minjus.gob.pe'); /*Defensor*/

/*  tbl_usuario */
CREATE TABLE `tbl_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_persona_id` int(11)  DEFAULT NULL,
  `tbl_rol_id` int(11) NOT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `clave` varchar(40) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `estado_app` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_usuario` (tbl_persona_id,`tbl_rol_id`, `usuario`, `clave`, `estado`) VALUES(1,1, 'jalexis', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1);   /* SUPER ADMIN - 123456 */
INSERT INTO `tbl_usuario` (tbl_persona_id,`tbl_rol_id`, `usuario`, `clave`, `estado`) VALUES(2,4, 'rzambrano', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1); /* EDITOR - 123456 */
INSERT INTO `tbl_usuario` (tbl_persona_id,`tbl_rol_id`, `usuario`, `clave`, `estado`) VALUES(3,4, 'cpariona', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1);  /* EDITOR - 123456 */
INSERT INTO `tbl_usuario` (tbl_persona_id,`tbl_rol_id`, `usuario`, `clave`, `estado`) VALUES(4,4, 'jzavala', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1);   /* EDITOR - 123456 */
INSERT INTO `tbl_usuario` (tbl_persona_id,`tbl_rol_id`, `usuario`, `clave`, `estado`) VALUES(5,3, 'rperez', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1);   /* EVALUADOR - 123456 */


/* tbl_tipo_evaluacion */
CREATE TABLE tbl_evaluacion (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_cargo_id int(11) NOT NULL,
  descripcion varchar(100),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_evaluacion (tbl_cargo_id,descripcion) VALUES(2,'Ficha para ser llenado por el Director Distrital');
INSERT INTO tbl_evaluacion (tbl_cargo_id,descripcion) VALUES(3,'Ficha para ser llenado por el Administrador Distrital');
INSERT INTO tbl_evaluacion (tbl_cargo_id,descripcion) VALUES(6,'Ficha para ser llenado por el Defensor Público');

/* tbl_evaluacion_titulo */
CREATE TABLE tbl_evaluacion_titulo (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_evaluacion_id int(11) NOT NULL,
  descripcion varchar(100),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'I. DIFUSIÓN DEL REGISTRO');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'II. ARTICULACIÓN INSTITUCIONAL');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'III.PADRON PRELIMINAR DE PRESUNTAS VÍCTIMAS');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'IV.PADRON CONSOLIDADO DE PRESUNTAS VÍCTIMAS');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'V.COORDINACIÓN CON EL SECTOR SALUD PARA LA EMISIÓN DE INFORMES MEDICOS');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'VI. OBSERVACIONES DEL DIRECTOR');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'VII. OBSERVACIONES DEL EVALUADOR');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(1,'VIII. EVALUACIÓN');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(2,'IX. DIFUSIÓN DEL REGISTRO');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(2,'X. EVALUACIÓN');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(3,'XI. REGISTRO DE INFORMACIÓN');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(3,'XII. ENTREGA DE INFORMACIÓN A LA DIRECCIÓN DISTRITAL');
INSERT INTO tbl_evaluacion_titulo (tbl_evaluacion_id,descripcion) VALUES(3,'XIII. EVALUACIÓN');

/* tbl_evaluacion_contenido */
CREATE TABLE tbl_evaluacion_detalle (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_evaluacion_titulo_id int(11) NOT NULL,
  descripcion varchar(100),
  descripcion_informativo varchar(200),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'1.Material informativo utilizado','(Castellano, Quechua y/u otro dialecto)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'2.Actividades de difusión','(Charlas, Campañas informativa, material informativo,  invitaciones a autoridades,  representantes de gremios ONG (s), etc)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'3.Publicidad escrita','(Castellano, Quechua, Otro Dialecto)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'4.Publicidad radial','(Castellano, Quechua, Otro Dialecto)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'5.Publicidad televisiva','(Castellano, Quechua, Otro Dialecto)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'6.Publicidad por redes sociales','(Castellano, Quechua, Otro Dialecto)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'7.Entrevistas','(Federaciones, Asociaciones, etc.)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(1,'8.Otros','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'1.Director Distrital','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'2.Gobierno Regional','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'3.Alcalde Distrital','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'4.MIMP','(Centro Emergencia Mujer)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'5.MINSA','(Comunidades - Anexos)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'6.MINCULT',' (Traductores)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'7.ONG(s)','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'8.Organizaciones de la sociedad civil ','(Gremios,  asociaciones, etc)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'9.Jueces de Paz','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'10.Tenientes gobernadores','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'11.Rondas campesinas, comunidades nativas','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(2,'12.Otros','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(8,'Calificación del cumplimiento de la entrega del padron de presuntas víctimas.','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(8,'Calificación de las actividades de Difusión del Registro','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(8,'Calificación de la articulación interinstitucional.','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(8,'Calificación de la coordinación con el sector salud para evaluación medica','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'1.Local','(Ubicación, documentos de coordinación)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'2.Caracteristicas del local','(Ambientes)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'2.Mobiliario','(Sillas, escritorio, mesas, otros)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'3.Computadoras / Laptop´s','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'4. Impresora / impresora multifuncional / escanner','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'5.Materiales de escritorio','(Papel, folderes, lapiceros, tonner, entre otros)');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'6.Gestion de viaticos','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'7.Gastos por caja chica','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(9,'8.Otros','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(10,'Calificación de la habilitación del ambiente para la inscripción en el Registro de Víctimas de Esterilización Forzada.','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(11,'1.Ficha de Registro','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(11,'2.Ficha de derivación Médica','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(11,'3.Recepción Examen Médico','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(12,'1.Informe final de atenciones','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(12,'2.Acta de entrega de carpetas y/o documentación relevante.','');
INSERT INTO tbl_evaluacion_detalle (tbl_evaluacion_titulo_id,descripcion,descripcion_informativo) VALUES(13,'Calificación del cumplimiento de responsabilidades.','');
























/* tbl_inscripcion_itinerante (Cabecera) */
CREATE TABLE tbl_inscripcion_itinerante (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_tipo_evaluacion_id int(11) DEFAULT NULL,
  lugar varchar(200) DEFAULT  NULL,
  fecha TIMESTAMP NULL DEFAULT  CURRENT_TIMESTAMP,
  tbl_persona_id int(11) NOT NULL,
  tbl_distrito_judicial int(11) NOT NULL
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* tbl_herramienta */
CREATE TABLE tbl_herramienta (
  id int(11) NOT NULL AUTO_INCREMENT,
  descripicion varchar(100),
  detalle_descripicion varchar(100),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* tbl_herramienta_inscripcion (Detalle) */
CREATE TABLE tbl_herramienta_inscripcion (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_inscripcion_itinerante_id int(11) NOT NULL,
  tbl_herramienta_id int(11) NOT NULL,
  especificacion varchar(200),
  medio_verificacion varchar(200),
  cumple int(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* tbl_articulacion_institucional */
CREATE TABLE tbl_articulacion_institucional (
  id int(11) NOT NULL AUTO_INCREMENT,
  descripicion varchar(100),
  detalle_descripicion varchar(100),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* tbl_articulacion_institucional_inscripcion (Detalle) */
CREATE TABLE tbl_articulacion_institucional_inscripcion (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_inscripcion_itinerante_id int(11) NOT NULL,
  tbl_articulacion_institucional_id int(11) NOT NULL,
  especificacion varchar(200),
  cumple int(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* tbl_evaluacion_estado */
CREATE TABLE tbl_evaluacion_estado (
  id int(11) NOT NULL AUTO_INCREMENT,
  descripcion varchar(50),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_evaluacion_estado(descripcion) VALUES('MALO');
INSERT INTO tbl_evaluacion_estado(descripcion) VALUES('REGULAR');
INSERT INTO tbl_evaluacion_estado(descripcion) VALUES('BUENO');
INSERT INTO tbl_evaluacion_estado(descripcion) VALUES('MUY BUENO');

/* tbl_evaluacion */
CREATE TABLE tbl_evaluacion (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_cargo_id int(11) NOT NULL,
  descripcion varchar(100),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación del cumplimiento de la entrega del padron de presuntas víctimas.',2);
INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación de las actividades de Difusión del Registro.',2);
INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación de la articulación interinstitucional.',2);
INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación de la coordinación con el sector salud para evaluación medica.',2);
INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación de la habilitación del ambiente para la inscripción en el Registro de Víctimas de Esterilización Forzada.',3);
INSERT INTO tbl_evaluacion(descripcion,tbl_cargo_id) VALUES('Calificación del cumplimiento de responsabilidades.',6);

/* tbl_evaluacion_inscripcion (Detalle) */
CREATE TABLE tbl_evaluacion_inscripcion (
  id int(11) NOT NULL AUTO_INCREMENT,
  tbl_inscripcion_itinerante_id int(11) NOT NULL,
  tbl_evaluacion_id int(11) NOT NULL,
  tbl_evaluacion_estado_id int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



